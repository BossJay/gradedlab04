import java.util.Scanner;

public class Refrigerator
{
  final Scanner SCAN = new Scanner(System.in);
  private int shelves;
  private int drawers;
  private double height;
  
  public Refrigerator(int inputShelves, int inputDrawers, double inputHeight)
  {
    this.shelves = inputShelves;
    this.drawers = inputDrawers;
    this.height = inputHeight;
  }
  
  public void setShelves(int newShelves)
  {
    this.shelves = newShelves;
  }
  
  public void setDrawers(int newDrawers)
  {
    this.drawers = newDrawers;
  }
  
  public void setHeight(double newHeight)
  {
    this.height = newHeight;
  }
  
  public int getShelves()
  {
    return this.shelves;
  }
  
  public int getDrawers()
  {
    return this.drawers;
  }
  
  public double getHeight()
  {
    return this.height;
  }

  public void howManyShelves()
  {
    System.out.println("Number of shelves: " + this.shelves);
  }

  public void howManyDrawers()
  {
    System.out.println("Number of drawers: " + this.drawers);
  }

  public void numOfShelves(int shelvesInput)
  {
    int validatedInput = validateInput(shelvesInput);
    this.shelves = validatedInput;
  }

  public int validateInput(int newShelves)
  {
    int validInput = 0;
    
    if (newShelves >= 2)
    {
      validInput = newShelves;
    }
    else
    {
      while (newShelves < 2)
      {
      System.out.println("Sorry! You need to get 2 or more shelves!");
      newShelves = SCAN.nextInt();
      }      
      validInput = newShelves;
    }
   
    return validInput;
  }
}
