import java.util.Scanner;

public class ApplianceStore
{
  public static void main(String[] args)
  {
    Refrigerator[] fridges = new Refrigerator[4];
    Scanner scan = new Scanner(System.in);

    for (int i = 0; i < fridges.length; i++)
    {
      System.out.println("How many shelves?");
      int shelves = scan.nextInt();

      System.out.println("How many drawers?");
      int drawers = scan.nextInt();

      System.out.println("What height?");
      double height = scan.nextDouble();
     
      fridges[i] = new Refrigerator(shelves, drawers, height);
    }
    
    System.out.println("Regarding the 2nd Fridge:");
    fridges[1].numOfShelves(-3);
    System.out.println("Number of shelves for the 2nd fridge: " + fridges[1].getShelves());
    
    System.out.println("Shelves: " + fridges[3].getShelves());
    System.out.println("Drawers: " + fridges[3].getDrawers());
    System.out.println("Height: " + fridges[3].getHeight());
    
    System.out.println("How many shelves?");
    fridges[3].setShelves(scan.nextInt());
    System.out.println("How many drawers?");
    fridges[3].setDrawers(scan.nextInt());
    System.out.println("What height?");
    fridges[3].setHeight(scan.nextDouble());
    
    System.out.println("Shelves: " + fridges[3].getShelves());
    System.out.println("Drawers: " + fridges[3].getDrawers());
    System.out.println("Height: " + fridges[3].getHeight());

    fridges[0].howManyShelves();
    fridges[0].howManyDrawers();
    
  }
}